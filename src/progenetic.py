import random

#algoritma genetika step by step
class progenetic:

    def __init__(self, df_food, df_lauk, df_sayur, df_buah, df_snack, physic_act, my_composition):
        self.df_food = df_food
        self.df_lauk = df_lauk
        self.df_sayur = df_sayur
        self.df_buah = df_buah
        self.df_snack = df_snack
        self.physic_act = physic_act
        self.my_composition = my_composition

#kromosom
#menu pagi,siang,malam
    # def generate_kromosom(self):
    #     kromosom = {}
    #     gen = [random.randint(0, len(self.df_food)-1) for _ in range(14)]
    #     kromosom['gen'] = gen
    #     object_func = self.object_function(kromosom)
    #     kromosom['fitness'] = self.fitness_function(object_func)
    #     return kromosom

#inisialisasi kromosom pertama dalam seminggu
#google jam page2
    # ===============JATAH SEMINGGU============
    #14 x 7 = 98
    def generate_kromosom_week(self):
        kromosom = {}
        gen = [random.randint(0, len(self.df_food)-1) for _ in range(98)]
        kromosom['gen'] = gen
        object_func = self.object_function(kromosom)
        kromosom['fitness'] = self.fitness_function(object_func)
        return kromosom

#referensi ibu hamil 4) Evaluasi page 5
    def object_function(self, kromosom):
        kal_sum = []
        protein_sum = []
        lemak_sum = []
        karbo_sum = []
        #[2, 3, 12, 32, 43, 34 ,53, 34,12, 1, 11, 12, 14, 14]
        for ix in range(len(kromosom['gen'])):
            #pokok
            if ix == 0 or ix == 5 or ix == 10 or ix == 14 or ix == 19 or ix == 24 or ix == 28 or ix == 33 or ix == 38 or ix == 42 or ix == 47 or ix == 52 or ix == 56 or ix == 61 or ix == 66 or ix == 70 or ix == 75 or ix == 80 or ix == 84 or ix == 89 or ix == 94:
                kal_sum.append(self.df_food.iloc[kromosom['gen'][ix], 2])
                protein_sum.append(self.df_food.iloc[kromosom['gen'][ix], 4])
                lemak_sum.append(self.df_food.iloc[kromosom['gen'][ix], 5])
                karbo_sum.append(self.df_food.iloc[kromosom['gen'][ix], 3])
                #lauk
            elif ix == 1 or ix == 6 or ix == 11 or ix == 15 or ix == 20 or ix == 25 or ix == 29 or ix == 34 or ix == 39 or ix == 43 or ix == 48 or ix == 53 or ix == 57 or ix == 62 or ix == 67 or ix == 71 or ix == 76 or ix == 81 or ix == 85 or ix == 90 or ix == 95:
                kal_sum.append(self.df_lauk.iloc[kromosom['gen'][ix], 2])
                protein_sum.append(self.df_lauk.iloc[kromosom['gen'][ix], 4])
                lemak_sum.append(self.df_lauk.iloc[kromosom['gen'][ix], 5])
                karbo_sum.append(self.df_lauk.iloc[kromosom['gen'][ix], 3])
                #sayur
            elif ix == 2 or ix == 7 or ix == 12 or ix == 16 or ix == 21 or ix == 26 or ix == 30 or ix == 35 or ix == 40 or ix == 44 or ix == 49 or ix == 54 or ix == 58 or ix == 63 or ix == 68 or ix == 72 or ix == 77 or ix == 82 or ix == 86 or ix == 91 or ix == 96:
                kal_sum.append(self.df_sayur.iloc[kromosom['gen'][ix], 2])
                protein_sum.append(self.df_sayur.iloc[kromosom['gen'][ix], 4])
                lemak_sum.append(self.df_sayur.iloc[kromosom['gen'][ix], 5])
                karbo_sum.append(self.df_sayur.iloc[kromosom['gen'][ix], 3])
                #buah
            elif ix == 3 or ix == 8 or ix == 13 or ix == 17 or ix == 22 or ix == 27 or ix == 31 or ix == 36 or ix == 41 or ix == 45 or ix == 50 or ix == 55 or ix == 59 or ix == 64 or ix == 69 or ix == 73 or ix == 78 or ix == 83 or ix == 87 or ix == 92 or ix == 97:
                kal_sum.append(self.df_buah.iloc[kromosom['gen'][ix], 2])
                protein_sum.append(self.df_buah.iloc[kromosom['gen'][ix], 4])
                lemak_sum.append(self.df_buah.iloc[kromosom['gen'][ix], 5])
                karbo_sum.append(self.df_buah.iloc[kromosom['gen'][ix], 3])
                #snack
            elif ix == 4 or ix == 9 or ix == 18 or ix == 23 or ix == 32 or ix == 37 or ix == 46 or ix == 51 or ix == 60 or ix == 65 or ix == 74 or ix == 79 or ix == 88 or ix == 93:
                kal_sum.append(self.df_snack.iloc[kromosom['gen'][ix], 2])
                protein_sum.append(self.df_snack.iloc[kromosom['gen'][ix], 4])
                lemak_sum.append(self.df_snack.iloc[kromosom['gen'][ix], 5])
                karbo_sum.append(self.df_snack.iloc[kromosom['gen'][ix], 3])

        p_kalori = round(abs(self.physic_act - sum(kal_sum)), 2)
        p_protein = round(abs(self.my_composition[0] - sum(protein_sum)), 2)
        p_lemak = round(abs(self.my_composition[1] - sum(lemak_sum)), 2)
        p_karbo = round(abs(self.my_composition[2] - sum(karbo_sum)), 2)
        return round(p_kalori + p_protein + p_lemak + p_karbo, 2)

#rumus fitness
    def fitness_function(self, obj_func):
        return round(1 / (1 + obj_func), 9)

#population = 8
    def generate_population(self, num_population):
        population = [self.generate_kromosom_week()
                      for _ in range(num_population)]
        return population

#rank selection
    def parent_selection(self, population):
        population = sorted(
            population, key=lambda x: x['fitness'], reverse=True)
        return population, [population[0], population[1]]

#singlepoint random point 0-3
    def crossover(self, parents, random_point=4):
        d_child1 = parents[0]
        d_child2 = parents[1]
        child1 = {}
        child2 = {}
        child1['gen'] = d_child1['gen'][:random_point] + \
            d_child2['gen'][random_point:]
        child2['gen'] = d_child2['gen'][:random_point] + \
            d_child1['gen'][random_point:]
        child1['fitness'] = d_child1['fitness']
        child2['fitness'] = d_child2['fitness']
        return [child1, child2]

#mutasi probabilitas = 0.3
    def mutation(self, childs, mutation_num=0.3):
        all_mutants = []
        for child in childs:
            mutant = {}
            for ix in range(len(child['gen'])):
                if random.random() < mutation_num:
                    child['gen'][ix] = random.randint(0, len(self.df_food)-1)
                mutant['gen'] = child['gen']
            mutant['fitness'] = child['fitness']
            obj_func_mutant = self.object_function(mutant)
            mutant['fitness'] = self.fitness_function(obj_func_mutant)
            all_mutants.append(mutant)
        return all_mutants


    def regeneration(self, populasi, mutants):
        del populasi[-2:]
        for mutant in mutants:
            populasi.append(mutant)
        new_population = populasi
        return new_population
