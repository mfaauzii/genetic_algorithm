
class probenedict:

    def __init__(self, jenis_kelamin, umur, berat_badan, tinggi_badan):
        self.jenis_kelamin = jenis_kelamin
        self.umur = umur
        self.berat_badan = berat_badan
        self.tinggi_badan = tinggi_badan

    def get_amb(self):
        amb = 0
        if self.jenis_kelamin == "p":
            amb = 655 + (9.6 * self.berat_badan) + \
                (1.8 * self.tinggi_badan) - (4.7 * self.umur)
        elif self.jenis_kelamin == "l":
            amb = 66 + (13.7 * self.berat_badan) + \
                (5 * self.tinggi_badan) - (6.8 * self.umur)
        return amb

    def get_physic_act(self, amb, weight):
        if self.jenis_kelamin == "p" and weight == 1:
            return round(1.3 * amb)
        elif self.jenis_kelamin == "p" and weight == 2:
            return round(1.55 * amb)
        elif self.jenis_kelamin == "p" and weight == 3:
            return round(1.7 * amb)
        elif self.jenis_kelamin == "p" and weight == 4:
            return round(2.0 * amb)

        if self.jenis_kelamin == "l" and weight == 1:
            return round(1.3 * amb)
        elif self.jenis_kelamin == "l" and weight == 2:
            return round(1.65 * amb)
        elif self.jenis_kelamin == "l" and weight == 3:
            return round(1.76 * amb)
        elif self.jenis_kelamin == "l" and weight == 4:
            return round(2.1 * amb)

    def get_composition(self, physic_act, protein=0.15, lemak=0.2, karbohidrat=0.65):
        comp_protein = round(protein * physic_act / 4, 2)
        comp_lemak = round(lemak * physic_act / 9, 2)
        comp_karbo = round(karbohidrat * physic_act / 4, 2)
        return comp_protein, comp_lemak, comp_karbo
