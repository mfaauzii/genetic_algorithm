from flask import Blueprint
from flask_restful import Api
from resources.ga_class import GeneticAlgorithm

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# route
api.add_resource(GeneticAlgorithm, '/setMenu')
