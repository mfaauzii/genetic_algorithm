from flask_restful import Resource, reqparse
from flask import jsonify, request
from coreGA import *


class GeneticAlgorithm(Resource):

    def get(self):
        return {"message": "Welcome Genetic"}

    def post(self):
        #request.json hit api in postman body
        #request.form hit api in postman form
        gander = request.form['gender']
        age = request.form['age']
        height = request.form['height']
        weight = request.form['weight']
        act = request.form['type']
        generator = PythonGA()
        fitn, benedict, output = generator.calculate(
            gander, age, weight, height, act)
        return {"fitness": fitn, "benedict": benedict, "menu": output}