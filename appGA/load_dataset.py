import pandas as pd


def load_makanan_pokok():
    df_pokok = pd.read_excel('./datasets/pokok.xls')
    return df_pokok


def load_lauk_pauk():
    df_lauk = pd.read_excel('./datasets/lauk.xls')
    return df_lauk


def load_buah():
    df_buah = pd.read_excel('./datasets/buah.xls')
    return df_buah


def load_sayur():
    df_sayur = pd.read_excel('./datasets/sayur.xls')
    return df_sayur


def load_snack():
    df_snack = pd.read_excel('./datasets/snack.xls')
    return df_snack
