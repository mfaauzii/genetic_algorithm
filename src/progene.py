# import random
# from os import system, name
# from time import sleep


# def clear():
#     if name == "nt":
#         _ = system('cls')
#     else:
#         _ = system('clear')


# target = 'mohamad noor fauzi'


# class progene:

#     def __init__(self, target):
#         self.target = target

#     def generate_gen(self):
#         new_gen = {}
#         gene = [chr(random.randint(32, 126)) for _ in range(len(self.target))]
#         fitness = 0
#         for char in range(len(self.target)):
#             if gene[char] == self.target[char]:
#                 fitness += 1
#         fitness = fitness / len(self.target) * 100.0
#         new_gen['gen'] = gene
#         new_gen['fitness'] = fitness
#         return new_gen

#     def get_fitness(self, mutants):
#         for mutant in mutants:
#             fitness = 0
#             for ix in range(len(mutant['gen'])):
#                 if mutant['gen'][ix] == self.target[ix]:
#                     fitness += 1
#             fitness = fitness / len(self.target) * 100.0
#             mutant['fitness'] = fitness
#         return mutants

#     def generate_population(self, number):
#         population = [self.generate_gen() for _ in range(number)]
#         return population

#     def parent_selection(self, population):
#         population = sorted(
#             population, key=lambda x: x['fitness'], reverse=True)
#         return population, [population[0], population[1]]

#     def crossover(self, parents):
#         d_child1 = parents[0]
#         d_child2 = parents[1]
#         child1 = {}
#         child2 = {}
#         child1['gen'] = d_child1['gen'][:1] + d_child2['gen'][1:]
#         child2['gen'] = d_child2['gen'][:1] + d_child1['gen'][1:]
#         child1['fitness'] = d_child1['fitness']
#         child2['fitness'] = d_child2['fitness']
#         return [child1, child2]

#     def mutation(self, childs, mutation_num=0.2):
#         all_mutants = []
#         for child in childs:
#             mutant = {}
#             for im in range(len(child['gen'])):
#                 if random.random() < mutation_num:
#                     child['gen'][im] = chr(random.randint(32, 126))
#                 mutant['gen'] = child['gen']
#                 mutant['fitness'] = child['fitness']
#             all_mutants.append(mutant)
#         return all_mutants

#     def regeneration(self, population, children):
#         del population[-2:]
#         for imutant in range(len(children)):
#             population.append(children[imutant])
#         new_population = population
#         return new_population


# GA = progene(target)
# population = GA.generate_population(5)
# condition = True
# recomendation = ''
# iteration = 1
# while condition:
#     population, parents = GA.parent_selection(population)
#     childs = GA.crossover(parents)
#     mutants = GA.mutation(childs, 0.01)
#     fitted_mutant = GA.get_fitness(mutants)
#     population = GA.regeneration(population, fitted_mutant)
#     _, bestSolution = GA.parent_selection(population)
#     print('generation : ', iteration, ''.join(bestSolution[0]['gen']),
#           ' : ', round(bestSolution[0]['fitness'], 2))
#     if bestSolution[0]['fitness'] == 100.0:
#         recomendation = ''.join(bestSolution[0]['gen'])
#         condition = False
#     iteration += 1
# print(recomendation)
