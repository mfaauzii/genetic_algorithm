"""
   FILE KODINGAN BACKEND
"""

from appGA import load_dataset
from src.probenedict import probenedict
from src.progenetic import progenetic
from os import system, name
'''
    parameter populasi
    -parameter crossover point <= 14
    -generasi
    -peluang mutasi
'''


def clear():
    if name == "nt":
        _ = system('cls')
    else:
        _ = system('clear')


class PythonGA:
    def calculate(self, gender, age, weight, height, act_type):
        max_generation = 500
        # prepare dataset
        df_food = load_dataset.load_makanan_pokok()
        df_lauk = load_dataset.load_lauk_pauk()
        df_sayur = load_dataset.load_sayur()
        df_buah = load_dataset.load_buah()
        df_snack = load_dataset.load_snack()

        # harris benedict function
        HB = probenedict(gender, int(age), int(weight), int(height))
        my_amb = HB.get_amb()
        my_AF = HB.get_physic_act(my_amb, int(act_type))
        my_composition = HB.get_composition(my_AF)

        GA = progenetic(df_food, df_lauk, df_sayur, df_buah,
                        df_snack, my_AF, my_composition)
        single = GA.generate_kromosom_week()
      #   print(single)
        population = GA.generate_population(8)
        generating = True
        generation = 1
        while generating:
            population, parents = GA.parent_selection(population)
            childs = GA.crossover(parents, 2)
            mutants = GA.mutation(childs)
            population = GA.regeneration(population, mutants)
            _, bestSolution = GA.parent_selection(population)
            print('generation : ', generation,
                  'fitness : ', bestSolution[0]['fitness'])
            clear()
            #program dapat berhenti dengan 2 kemungkinan
            if bestSolution[0]['fitness'] > 0.1 or generation >= max_generation:
                print(bestSolution[0]['fitness'])
                generating = False
            generation += 1
        hari = 1
        output = {}
        allDay = []
        for i in range(0, len(bestSolution[0]['gen'])-1, 14):
            total_cal = 0
            total_pro = 0
            total_lem = 0
            total_karb = 0

            pagi = []
            siang = []
            malam = []
            print('CEK AMB ' + str(my_amb))
            print('CEK INPUT ACT' + str(act_type))
            print('=======Hari ke - ' + str(hari) + '==========\nMakanan Pokok Pagi : ',
                  df_food.iloc[bestSolution[0]['gen'][i], 1])
            total_cal += df_food.iloc[bestSolution[0]['gen'][i], 2]
            total_karb += df_food.iloc[bestSolution[0]['gen'][i], 3]
            total_pro += df_food.iloc[bestSolution[0]['gen'][i], 4]
            total_lem += df_food.iloc[bestSolution[0]['gen'][i], 5]
            print('Lauk Pauk Pagi : ',
                  df_lauk.iloc[bestSolution[0]['gen'][i+1], 1])
            total_cal += df_lauk.iloc[bestSolution[0]['gen'][i+1], 2]
            total_karb += df_lauk.iloc[bestSolution[0]['gen'][i+1], 3]
            total_pro += df_lauk.iloc[bestSolution[0]['gen'][i+1], 4]
            total_lem += df_lauk.iloc[bestSolution[0]['gen'][i+1], 5]
            print('Sayur Pagi : ',
                  df_sayur.iloc[bestSolution[0]['gen'][i+2], 1])
            total_cal += df_sayur.iloc[bestSolution[0]['gen'][i+2], 2]
            total_karb += df_sayur.iloc[bestSolution[0]['gen'][i+2], 3]
            total_pro += df_sayur.iloc[bestSolution[0]['gen'][i+2], 4]
            total_lem += df_sayur.iloc[bestSolution[0]['gen'][i+2], 5]
            print('Buah Pagi : ',
                  df_buah.iloc[bestSolution[0]['gen'][i+3], 1])
            total_cal += df_buah.iloc[bestSolution[0]['gen'][i+3], 2]
            total_karb += df_buah.iloc[bestSolution[0]['gen'][i+3], 3]
            total_pro += df_buah.iloc[bestSolution[0]['gen'][i+3], 4]
            total_lem += df_buah.iloc[bestSolution[0]['gen'][i+3], 5]
            print('Snack Pagi : ',
                  df_snack.iloc[bestSolution[0]['gen'][i+4], 1])
            total_cal += df_snack.iloc[bestSolution[0]['gen'][i+4], 2]
            total_karb += df_snack.iloc[bestSolution[0]['gen'][i+4], 3]
            total_pro += df_snack.iloc[bestSolution[0]['gen'][i+4], 4]
            total_lem += df_snack.iloc[bestSolution[0]['gen'][i+4], 5]

            pagi.append(df_food.iloc[bestSolution[0]['gen'][i], 1])
            pagi.append(df_lauk.iloc[bestSolution[0]['gen'][i+1], 1])
            pagi.append(df_sayur.iloc[bestSolution[0]['gen'][i+2], 1])
            pagi.append(df_buah.iloc[bestSolution[0]['gen'][i+3], 1])
            pagi.append(df_snack.iloc[bestSolution[0]['gen'][i+4], 1])

            print('Makan Pokok Siang : ',
                  df_food.iloc[bestSolution[0]['gen'][i+5], 1])
            total_cal += df_food.iloc[bestSolution[0]['gen'][i+5], 2]
            total_karb += df_food.iloc[bestSolution[0]['gen'][i+5], 3]
            total_pro += df_food.iloc[bestSolution[0]['gen'][i+5], 4]
            total_lem += df_food.iloc[bestSolution[0]['gen'][i+5], 5]
            print('Lauk Pauk Siang : ',
                  df_lauk.iloc[bestSolution[0]['gen'][i+6], 1])
            total_cal += df_lauk.iloc[bestSolution[0]['gen'][i+6], 2]
            total_karb += df_lauk.iloc[bestSolution[0]['gen'][i+6], 3]
            total_pro += df_lauk.iloc[bestSolution[0]['gen'][i+6], 4]
            total_lem += df_lauk.iloc[bestSolution[0]['gen'][i+6], 5]
            print('Sayur Siang : ',
                  df_sayur.iloc[bestSolution[0]['gen'][i+7], 1])
            total_cal += df_sayur.iloc[bestSolution[0]['gen'][i+7], 2]
            total_karb += df_sayur.iloc[bestSolution[0]['gen'][i+7], 3]
            total_pro += df_sayur.iloc[bestSolution[0]['gen'][i+7], 4]
            total_lem += df_sayur.iloc[bestSolution[0]['gen'][i+7], 5]
            print('Buah Siang : ',
                  df_buah.iloc[bestSolution[0]['gen'][i+8], 1])
            total_cal += df_buah.iloc[bestSolution[0]['gen'][i+8], 2]
            total_karb += df_buah.iloc[bestSolution[0]['gen'][i+8], 3]
            total_pro += df_buah.iloc[bestSolution[0]['gen'][i+8], 4]
            total_lem += df_buah.iloc[bestSolution[0]['gen'][i+8], 5]
            print('Snack Siang : ',
                  df_snack.iloc[bestSolution[0]['gen'][i+9], 1])
            total_cal += df_snack.iloc[bestSolution[0]['gen'][i+9], 2]
            total_karb += df_snack.iloc[bestSolution[0]['gen'][i+9], 3]
            total_pro += df_snack.iloc[bestSolution[0]['gen'][i+9], 4]
            total_lem += df_snack.iloc[bestSolution[0]['gen'][i+9], 5]

            siang.append(df_food.iloc[bestSolution[0]['gen'][i+5], 1])
            siang.append(df_lauk.iloc[bestSolution[0]['gen'][i+6], 1])
            siang.append(df_sayur.iloc[bestSolution[0]['gen'][i+7], 1])
            siang.append(df_buah.iloc[bestSolution[0]['gen'][i+8], 1])
            siang.append(df_snack.iloc[bestSolution[0]['gen'][i+9], 1])

            print('Makanan Pokok Malam : ',
                  df_food.iloc[bestSolution[0]['gen'][i+10], 1])
            total_cal += df_food.iloc[bestSolution[0]['gen'][i+10], 2]
            total_karb += df_food.iloc[bestSolution[0]['gen'][i+10], 3]
            total_pro += df_food.iloc[bestSolution[0]['gen'][i+10], 4]
            total_lem += df_food.iloc[bestSolution[0]['gen'][i+10], 5]
            print('Lauk Pauk Malam : ',
                  df_lauk.iloc[bestSolution[0]['gen'][i+11], 1])
            total_cal += df_lauk.iloc[bestSolution[0]['gen'][i+11], 2]
            total_karb += df_lauk.iloc[bestSolution[0]['gen'][i+11], 3]
            total_pro += df_lauk.iloc[bestSolution[0]['gen'][i+11], 4]
            total_lem += df_lauk.iloc[bestSolution[0]['gen'][i+11], 5]
            print('Sayur Malam : ',
                  df_sayur.iloc[bestSolution[0]['gen'][i+12], 1])
            total_cal += df_sayur.iloc[bestSolution[0]['gen'][i+12], 2]
            total_karb += df_sayur.iloc[bestSolution[0]['gen'][i+12], 3]
            total_pro += df_sayur.iloc[bestSolution[0]['gen'][i+12], 4]
            total_lem += df_sayur.iloc[bestSolution[0]['gen'][i+12], 5]
            print('Buah Malam : ',
                  df_buah.iloc[bestSolution[0]['gen'][i+13], 1])
            total_cal += df_buah.iloc[bestSolution[0]['gen'][i+13], 2]
            total_karb += df_buah.iloc[bestSolution[0]['gen'][i+13], 3]
            total_pro += df_buah.iloc[bestSolution[0]['gen'][i+13], 4]
            total_lem += df_buah.iloc[bestSolution[0]['gen'][i+13], 5]

            malam.append(df_food.iloc[bestSolution[0]['gen'][i+10], 1])
            malam.append(df_lauk.iloc[bestSolution[0]['gen'][i+11], 1])
            malam.append(df_sayur.iloc[bestSolution[0]['gen'][i+12], 1])
            malam.append(df_buah.iloc[bestSolution[0]['gen'][i+13], 1])

            allDay.append({"hari "+str(hari): [
                          {"pagi": pagi}, {"siang": siang}, {"malam": malam}]})
            allDay.append({"rincian": {"kalori": total_cal,
                                       "karbo": total_karb,
                                       "protein": total_pro,
                                       "lemak": total_lem}})

            print("===================================")
            print('Total Kalori dalam sehari : ', total_cal)
            print('Total Karbo dalam sehari : ', total_karb)
            print('Total Protein dalam sehari : ', total_pro)
            print('Total Lemak dalam sehari : ', total_lem)
            hari += 1
        benedict = {"Kalori dibutuhkan": my_AF,
                    'Protein dibutuhkan': my_composition[0],
                    'Lemak dibutuhkan': my_composition[1],
                    'Karbo dibutuhkan': my_composition[2]}
        fitn = bestSolution[0]['fitness']
        return fitn, benedict, allDay

# GA = PythonGA()

# fit,benedict,allday = GA.calculate('l',22,60,170,1)
# print(fit,benedict,allday)