from app import api_bp
from flask import Flask

app = Flask(__name__)


app.register_blueprint(api_bp, url_prefix='/api')

if __name__ == "__main__":
    app.run(debug = True)
