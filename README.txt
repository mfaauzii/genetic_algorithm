        RUMUS HARRIS BENEDICT
Komponen yang membutuhkan kebutuhan energi
|
|--> Angka metabolisme basal (AMB)
|--> Jenis aktivitas fisik

Rumus AMB(J:JenisKelamin, U:Umur, BB:BeratBadan, TB:TinggiBadan)
|
|--> Laki-Laki = 66 + (13.7 x BB) + (5 x TB) - (6.8 x U)
|--> Perempuan = 655 + (9.6 x BB) + (1.8 x TB) - (4.7 x U)

Rumus aktivitas fisik(J:JenisKelamin, AMB, weight)
|                          L               P
|--> 1. Sangat ringan =   1.3             1.3
|--> 2. Ringan        =   1.65            1.55
|--> 3. Sedang        =   1.76            1.7
|--> 4. Berat         =   2.1              2
|
|-------> AF = Indeks x AMB


        Kandungan Gizi
Protein = 10 - 15 %
    |
    |--> 1 gram protein = 4 kkal
    |--> 10% x AF / 4 gram

Lemak = 20 - 30 %
    |
    |--> 1 gram lemak = 9 kkal
    |--> 20% x AF / 9 gram

Karbohidrat = 60 - 70 %
    |
    |--> 1 gram karbohidrat = 4 kkal
    |--> 70% x AF / 4 gram

Dataset
 |
 |======Makan (Pokok Lauk Pauk, Sayur Mayur) 
 |======Snack (Buah dan Jus, Makanan RIngan)
 |--> Dataset Makanan Pokok
 |--> Dataset Lauk Pauk
 |--> Dataset Sayur  Mayur
 |--> Dataset Buah Buahan dan Jus
 |--> Dataset Makanan Ringan

 Kromosom
 ||
 ||==>Makan Pagi (MP, LP, SM)
 ||==>Snack Pagi (BH, MR)
 ||=========================
 ||==>Makan Siang (MP, LP, SM)
 ||==>Snack Siang (BH, MR)
 ||=========================
 ||==>Makan Malam (MP, LP, SM, BH)

DKBM Keywords
lauk pauk	pokok	snack	buah	Sayur
    FP	     AP	    CP	     EP	     DP
    GP	     BP	    JP		 CP
    HP	     CP	    QP		

Struktur Dataset
==>id , nama_makanan  , kalori ,  karbo , protein , lemak

Struktur get_Composition function
==>protein, lemak, karbo

=====FORMULA=====
Fobj = p_kalori + p_protein + p_lemak + p_karbo
Fitne ss = 1 / (1+Fobj)

-->Single point crossover<--
-->Rank selection<--
-->Fixed/Random Mutation<--

Program Stack
1. pip3 install pandas
2. pip3 install numpy (optional, karna sudah 1 package sama pandas)
3. pip3 install flask
4. pip3 install flask_restful
5. pip requirements.txt

Webservice
pythonanywhere.com, jangan lupa install package di console, virtualenv/ install python=python3.7/ pip install flask dkk.
directory/mysite

